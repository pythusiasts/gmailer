from setuptools import setup, find_packages

version = '0.1.0'

requirements = [
    'pytest>=2.8.1',
    'pytest-mock>=0.7.0',
]

setup(
    name='gmailer',
    version=version,
    description="Thin gmail client.",
    author='Pythusiasts',
    author_email='pythusiasts@biarri.flowdock.com',
    packages=find_packages(exclude=['ez_setup', 'tests']),
    install_requires=requirements,
)
