import os
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders


class Gmailer(object):
    """
    Wrapper class to send emails using a gmail server.
    """
    def __init__(self, from_addr, password):
        """
        Initialise a Gmailer.

        :param from_addr: the email address to send mail from
        :param password: the password corresponding to the from_addr
        """
        self.from_addr = convert_to_ascii(from_addr)
        self.password = convert_to_ascii(password)
        self.server = None

    def send_mail(self, to_addr, message, subject=None, attachments=None):
        """
        Send an email to the given address.

        :param to_addr: the email address to send mail to
        :param message: the message to be sent as the body of the email
        :param subject: the subject line for the email
        """
        mail = self._create_mail(to_addr, message, subject)
        mail = self._add_attachments(mail, attachments)
        self._open_session()
        self.server.sendmail(self.from_addr, to_addr, mail.as_string())
        self._close_session()

    def _create_mail(self, to_addr, message, subject):
        """
        Create a server-ready email from the given message details.

        :param to_addr: the email address to send mail to
        :param message: the message to be sent as the body of the email
        :param subject: the subject line for the email
        :return: a server-ready email
        """
        message = convert_to_ascii(message)
        subject = convert_to_ascii(subject)

        mail = MIMEMultipart()
        mail["From"] = self.from_addr
        mail["To"] = to_addr
        mail["Subject"] = subject
        mail.attach(MIMEText(message, "plain"))

        return mail

    @staticmethod
    def _add_attachments(mail, attachments):
        """
        Add given file attachments to an email.

        :param mail: a server-ready email
        :param attachments: a list of files to be attached
        :return: the mail with the given attachments added
        """
        if not attachments:
            return mail

        try:
            for attachment_path in attachments:
                full_filepath = os.path.abspath(attachment_path)
                filename = os.path.basename(full_filepath)
                with open(full_filepath, "rb") as attachment:
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(attachment.read())
                    encoders.encode_base64(part)
                    part.add_header("Content-Disposition", "attachment; filename={}".format(filename))
                    mail.attach(part)

        except Exception, e:
            raise e

        return mail

    def _open_session(self):
        """
        Open a session to the server
        """
        self.server = smtplib.SMTP("smtp.gmail.com", 587)
        self.server.starttls()
        self.server.login(self.from_addr, self.password)

    def _close_session(self):
        """
        Close a session to the server
        """
        self.server.quit()


def convert_to_ascii(object):
    """
    Attempt to convert the given object into an ascii string.

    :param object: any given object
    :return: the ascii string representation of the given object
    """
    try:
        return str(object).encode('ascii', 'ignore')
    except:
        raise ValueError("Cannot convert object to ascii")
